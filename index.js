// Bài 1
function luong() {
    var luongNgay = 100000;
    var soNgay = document.getElementById("ngaylam").value * 1;
    var luongTong = luongNgay * soNgay;
    document.getElementById("luongTong").innerHTML = `<h5>Lương tổng: ${luongTong}</h5>`;
}

// Bài 2
function trungBinh() {
    var a = document.getElementById("soA").value * 1;
    var b = document.getElementById("soB").value * 1;
    var c = document.getElementById("soC").value * 1;
    var d = document.getElementById("soD").value * 1;
    var e = document.getElementById("soE").value * 1;
    var binhQuan = (a + b + c + d + e) / 5
    document.getElementById("binhQuan").innerHTML = `<h5>Giá trị bình quân: ${binhQuan}</h5>`
}

//bai 3
function quyDoi() {
    var vn = 23500;
    var usd = document.getElementById("usd").value * 1;
    var usdtovn = vn * usd;
    document.getElementById("usdtovn").innerHTML = `<h5>${usd}$ quy đổi ra: ${usdtovn}vnd</h5>`
}
// bài 4

function chuVi() {
    var dai = document.getElementById("chieuDai").value * 1;
    var rong = document.getElementById("chieuRong").value * 1;
    var cv = (dai + rong) * 2;
    document.getElementById("chuvi").innerHTML = `<h5>Chu vi HCN: ${cv}</h5>`
}
function dienTich() {
    var dai = document.getElementById("chieuDai").value * 1;
    var rong = document.getElementById("chieuRong").value * 1;
    var dt = dai * rong;
    document.getElementById("dientich").innerHTML = `<h5>Diện tích HCN: ${dt}</h5>`
}

// bài 5
function tong() {
    var so = document.getElementById("so").value * 1;
    var donVi = so % 10;
    var chuc = Math.floor(so / 10);
    var kySo = donVi + chuc;
    document.getElementById("kySo").innerHTML=`<h5>Tổng 2 ký số là: ${kySo}</h5>`;
}